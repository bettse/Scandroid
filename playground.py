import sys
from scanfuncs import *			# the Scansion Machine
from scanstrings import *		# some global texts & the Explainer

class Notes:
    def AppendText(self, text):
        pass

E = Explainer(Notes())

SM = ScansionMachine()		# central engine of scansion work
SM.SetLineFeet(5, True)

params = " ".join(sys.argv[1:])
print "%s" % params

SM.ParseLine(params)


(scanline, result) = SM.ShowSyllables(E)
#print "ShowSyllables"
#print scanline
#print "Scanline: %s\n\tResult: %s" % (scanline, result)
(scanline, result) = SM.ShowLexStresses(E)
#print "ShowLexStresses"
#print scanline
#print "Scanline: %s\n\tResult: %s" % (scanline, result)
(scanline, result) = SM.ChooseAlgorithm(E)
#print "ChooseAlgorithm"
#print scanline
#print "Scanline: %s\n\tResult: %s" % (scanline, result)
(scanline, result) = SM.WeirdEnds(E)
#print "WeirdEnds"
#print scanline
#print "Scanline: %s\n\tResult: %s" % (scanline, result)
(scanline, result) = SM.TestLengthAndDice(E)
#print "TestLengthAndDice"
#print scanline
#print "Scanline: %s\n\tResult: %s" % (scanline, result)
(scanline, result) = SM.PromotePyrrhics(E)
#print "PromotePyrrhics"
#print scanline
#print "Scanline: %s\n\tResult: %s" % (scanline, result)
(scanline, result) = SM.HowWeDoing(E)
#print "HowWeDoing"
print scanline
#print "Scanline: %s\n\tResult: %s" % (scanline, result)
#print SM.ChooseAlgorithm(E, deducingParams=True)
